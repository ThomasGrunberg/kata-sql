# Data model

## Archive table: KSQL_ARCHIVE

This table is a raw extraction of the NASA data. Its structure detail can be found [here](DATAMODEL_ARCHIVE.md).

## Stars table: KSQL_STARS

This table is a refined detail of the NASA data concerning stars with known exoplanets. Its structure detail can be found [here](DATAMODEL_STARS.md).

## Planets table: KSQL_EXOPLANETS

This table is a refined detail of the NASA data concerning known exoplanets. Its structure detail can be found [here](DATAMODEL_EXOPLANETS.md).
