package org.kata.katasql.file;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Collection;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class CSVFileReaderTest {
	@Test
	public void readFileAndDecode() {
		Collection<Map<String, String>> decodedFileContent = new CSVFileReader().readFileAndDecode(CSVFileReader.NASA_EXOPLANET_ARCHIVE_FILE);
		assertFalse(decodedFileContent.isEmpty());
		assertEquals(35_131, decodedFileContent.size());
	}
}
