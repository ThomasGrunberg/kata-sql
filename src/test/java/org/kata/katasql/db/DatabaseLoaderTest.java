package org.kata.katasql.db;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.jupiter.api.Test;

public class DatabaseLoaderTest {
	@Test
	public void loadFromFile() throws SQLException {
		Connection connection = H2DatabaseConnector.getConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT count(*) FROM " + DatabaseLoader.ARCHIVE_TABLE_NAME);
		assertTrue(rs.next());
		assertEquals(35_131, rs.getInt(1));
	}
	@Test
	public void aggregatedDataExoplanets() throws SQLException {
		Connection connection = H2DatabaseConnector.getConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT count(*) FROM " + DatabaseLoader.EXOPLANETS_TABLE_NAME);
		assertTrue(rs.next());
		assertEquals(5_539, rs.getInt(1));
	}
	@Test
	public void aggregatedDataStars() throws SQLException {
		Connection connection = H2DatabaseConnector.getConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT count(*) FROM " + DatabaseLoader.STARS_TABLE_NAME);
		assertTrue(rs.next());
		assertEquals(4_128, rs.getInt(1));
	}
}
