package org.kata.katasql.exercises;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.kata.katasql.db.H2DatabaseConnector;
import org.kata.katasql.file.SQLFileReader;

public class ExercisesTest {
	private static final String VERIFY_EXOPLANETS_TABLE = "SELECT COUNT(pl_name) FROM KSQL_EXOPL";
	private static final String VERIFY_STARS_TABLE = "SELECT COUNT(hostname) FROM KSQL_EXOSTARS";
	
	private Connection connection;
	
	private Connection getConnection() {
		if(connection == null) {
			connection = H2DatabaseConnector.getConnection();
		}
		return connection;
	}
	
	private boolean testScript(String fileName, String exercise,
			int expectedLines, int expectedFirstColumnValue
			) {
		String submittedQuery = new SQLFileReader().readQuery("exercises/" + fileName);
		if(submittedQuery == null || submittedQuery.isBlank())
			return false;
		int resultCount = 0;
		try (ResultSet result = getConnection().createStatement().executeQuery(submittedQuery)) {
			if(expectedLines > 0) {
				assertTrue(result.next(), "The query provides no result.");
				resultCount = result.getInt(1);
				if(expectedLines == 1)
					assertFalse(result.next(), "The query provides more than one result.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			assertNull(e, "The query is incorrect:" + submittedQuery + "\n" + e.getMessage());
		}
		if(expectedLines > 0) {
			assertEquals(expectedFirstColumnValue, resultCount, "The result is incorrect.");
			System.out.println("Exercise " + exercise + " completed successfully.");
		}
		return true;
	}
	
	private void testQuery(String submittedQuery, String exercise,
			int expectedLines, int expectedFirstColumnValue
			) {
		int resultCount = 0;
		try (ResultSet result = getConnection().createStatement().executeQuery(submittedQuery)) {
			if(expectedLines > 0) {
				assertTrue(result.next(), "The query provides no result.");
				resultCount = result.getInt(1);
				if(expectedLines == 1)
					assertFalse(result.next(), "The query provides more than one result.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			assertNull(e, "The query is incorrect:" + submittedQuery + "\n" + e.getMessage());
		}
		if(expectedLines > 0) {
			assertEquals(expectedFirstColumnValue, resultCount, "The result is incorrect.");
			System.out.println("Exercise " + exercise + " completed successfully.");
		}
	}
	
	private boolean testScript(String fileName, String exercise) {
		String submittedQuery = new SQLFileReader().readQuery("exercises/" + fileName);
		if(submittedQuery == null || submittedQuery.isBlank())
			return false;
		try {
			getConnection().createStatement().execute(submittedQuery);
		} catch (SQLException e) {
			e.printStackTrace();
			assertNull(e, "The query is incorrect:" + submittedQuery + "\n" + e.getMessage());
		}
		return true;
	}
	
	@Test
	public void exercise1CountPlanets() {
		testScript("exercise1-countplanets.sql", "1", 1, 5_539);
	}
	
	@Test
	public void exercise2CreatePlanetsTable() {
		if(testScript("exercise2-createplanettable.sql", "2"))
			testQuery(VERIFY_EXOPLANETS_TABLE, "2 (planets)", 1, 5_539);
	}
	
	@Test
	public void exercise2CreateStarsTable() {
		if(testScript("exercise2-createstartable.sql", "2"))
			testQuery(VERIFY_STARS_TABLE, "2 (star)", 1, 4_128);
	}

	@Test
	public void exercise3CountPlanetsAroundYellowDwarves() {
		testScript("exercise3-planetsaroundyellowdwarves.sql", "3", 1, 36);
	}

	@Test
	public void exercise4CountPlanetsWithTerrestrialMass() {
		testScript("exercise4-countplanetswithterrestrialmass.sql", "4", 1, 55);
	}

	@AfterEach
	public void cleanUp() throws SQLException {
		getConnection().rollback();
	}
}
