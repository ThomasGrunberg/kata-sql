package org.kata.katasql.db;

enum ColumnAggregationMethod {
	KEEP_AVG,
	KEEP_MAX,
	KEEP_MIN,
	KEEP_MAX_AS_ERROR,
	KEEP_MIN_AS_ERROR,
	NO_AGGREGATION;
}
