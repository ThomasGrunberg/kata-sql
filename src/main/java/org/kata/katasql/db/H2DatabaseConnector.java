package org.kata.katasql.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.h2.tools.Server;
import org.kata.katasql.file.CSVFileReader;

public final class H2DatabaseConnector {
	private static H2DatabaseConnector INSTANCE;
	
	private static synchronized H2DatabaseConnector getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new H2DatabaseConnector();
			Collection<Map<String, String>> decodedFileContent = new CSVFileReader().readFileAndDecode(CSVFileReader.NASA_EXOPLANET_ARCHIVE_FILE);
			Connection connection = INSTANCE.createConnection();
			DatabaseLoader dbLoader = new DatabaseLoader(connection);
			dbLoader.loadIntoDatabase(decodedFileContent);
			dbLoader.createDerivedTables();
			try {
				connection.commit();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return INSTANCE;
	}
	
	private H2DatabaseConnector() {
	}
	
	public static Connection getConnection() {
		return getInstance().createConnection();
	}
	
	private Connection createConnection() {
		try {
			Class.forName("org.h2.Driver");
			return DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "sa", "");
		}
		catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void showBrowser() {
		try {
			Server.startWebServer(getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
