package org.kata.katasql.db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseLoader {
	public static final String ARCHIVE_TABLE_NAME = "KSQL_ARCHIVE";
	public static final String EXOPLANETS_TABLE_NAME = "KSQL_EXOPLANETS";
	public static final String STARS_TABLE_NAME = "KSQL_STARS";
	
	private static final List<ColumnDefinition> PLANET_COLUMNS = List.of(
	         new ColumnDefinition("pl_name", String.class),
	         new ColumnDefinition("pl_letter", String.class),
	         new ColumnDefinition("hostname", "st_name", String.class),
	         new ColumnDefinition("disc_year", Integer.class, ColumnAggregationMethod.KEEP_MIN),
	         new ColumnDefinition("disc_pubdate", String.class, ColumnAggregationMethod.KEEP_MIN),
	         new ColumnDefinition("pl_orbper", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_orbpererr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_orbpererr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("pl_orbsmax", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_orbsmaxerr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_orbsmaxerr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("pl_rade", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_radeerr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_radeerr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("pl_masse", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_masseerr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_masseerr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("pl_dens", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_denserr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_denserr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("pl_orbeccen", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_orbeccenerr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_orbeccenerr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("pl_eqt", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("pl_eqterr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("pl_eqterr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR)
	         );
	
	private static final List<ColumnDefinition> STAR_COLUMNS = List.of(
	         new ColumnDefinition("hostname", "st_name", String.class),
	         new ColumnDefinition("sy_snum", Integer.class),
	         new ColumnDefinition("sy_pnum", Integer.class),
	         new ColumnDefinition("sy_mnum", Integer.class),
	         new ColumnDefinition("st_spectype", String.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("st_mass", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("st_masserr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("st_masserr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("st_met", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("st_meterr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("st_meterr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("st_age", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("st_ageerr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("st_ageerr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("sy_dist", BigDecimal.class, ColumnAggregationMethod.KEEP_AVG),
	         new ColumnDefinition("sy_disterr1", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX_AS_ERROR),
	         new ColumnDefinition("sy_disterr2", BigDecimal.class, ColumnAggregationMethod.KEEP_MIN_AS_ERROR),
	         new ColumnDefinition("rastr", String.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("ra", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("decstr", String.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("dec", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("glat", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("glon", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("elat", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX),
	         new ColumnDefinition("elon", BigDecimal.class, ColumnAggregationMethod.KEEP_MAX)
	         );
	
	private static final List<ColumnDefinition> MISC_COLUMNS = List.of(
	         new ColumnDefinition("discoverymethod", String.class),
	         new ColumnDefinition("pl_controv_flag", Boolean.class),
	         new ColumnDefinition("pl_masselim", Integer.class),
	         new ColumnDefinition("pl_orbperlim", Integer.class),
	         new ColumnDefinition("pl_orbsmaxlim", Integer.class),
	         new ColumnDefinition("pl_radelim", Integer.class),
	         new ColumnDefinition("pl_denslim", Integer.class),
	         new ColumnDefinition("pl_orbeccenlim", Integer.class),
	         new ColumnDefinition("pl_eqtlim", Integer.class),
	         new ColumnDefinition("st_masslim", Integer.class),
	         new ColumnDefinition("st_metlim", Integer.class),
	         new ColumnDefinition("st_agelim", Integer.class),
	         new ColumnDefinition("rowid", Integer.class)
	         );
	
	private final Connection dbConnection;
	
	public DatabaseLoader(Connection dbConnection) {
		this.dbConnection = dbConnection;
	}
	
	public void loadIntoDatabase(Collection<Map<String, String>> decodedFileContent) {
		createArchiveTable(decodedFileContent);
		fillArchiveTable(decodedFileContent);
		try {
			dbConnection.commit();
		} catch (SQLException e) {
			throw new RuntimeException("Error commiting into table " + ARCHIVE_TABLE_NAME, e);
		}
	}
	
	private void fillArchiveTable(Collection<Map<String, String>> decodedFileContent) {
		for(Map<String, String> decodedLine : decodedFileContent) {
			StringBuilder insertTableInstruction = new StringBuilder();
			insertTableInstruction.append("INSERT INTO ");
			insertTableInstruction.append(ARCHIVE_TABLE_NAME);
			StringBuilder into = new StringBuilder();
			StringBuilder values = new StringBuilder();
			Collection<ColumnDefinition> allColumns = new HashSet<>();
			allColumns.addAll(PLANET_COLUMNS);
			allColumns.addAll(STAR_COLUMNS);
			allColumns.addAll(MISC_COLUMNS);
			for(Entry<String, String> dataColumn : decodedLine.entrySet()) {
				if(dataColumn.getValue() == null || dataColumn.getValue().trim().equals(""))
					continue;
				Optional<ColumnDefinition> columnDef = allColumns.stream()
						.filter(c -> c.getColumnNameArchive().equals(dataColumn.getKey()))
						.findAny();
				if(columnDef.isEmpty())
					continue;
				Class<? extends Object> columnType = columnDef.get().getColumnType();
				if(columnType == null)
					continue;
				into.append(dataColumn.getKey());
				into.append(",");
				switch(columnType.getSimpleName()) {
					case "String" :		values.append("'" + dataColumn.getValue().replace("'", "''") + "'");	break;
					case "BigDecimal" :	
					case "Integer" :	values.append(dataColumn.getValue());				break;
					case "LocalDate" :	values.append("null");							break; // TODO
					default :			values.append("'" + dataColumn.getValue().replace("'", "''") + "'");	break;
				}
				values.append(",");
			}
			into.delete(into.length()-1, into.length());
			into.append(")\n");
			values.delete(values.length()-1, values.length());
			values.append(")");
			String insertQuery = insertTableInstruction.toString() 
					+ " (" + into.toString()
					+ " VALUES (" + values.toString();
			try {
				Statement statement = dbConnection.createStatement();
				statement.execute(insertQuery);
			} catch (SQLException e) {
				throw new RuntimeException("Error inserting into table " + ARCHIVE_TABLE_NAME + ": " + insertQuery, e);
			}
		}
	}
	
	public void createDerivedTables() {
		createDerivedTable(ARCHIVE_TABLE_NAME, EXOPLANETS_TABLE_NAME, PLANET_COLUMNS);
		createDerivedTable(ARCHIVE_TABLE_NAME, STARS_TABLE_NAME, STAR_COLUMNS);
	}

	private void createDerivedTable(String sourceTableName, String targetTableName, Collection<ColumnDefinition> columns) {
		StringBuilder createTableInstruction = new StringBuilder();
		String sourceColumnsName = columns.stream()
				.map(d -> d.getColumnNameAggregated())
				.collect(Collectors.joining(","));
		String groupByColumnsName = columns.stream()
				.filter(d -> d.getAggregationMethod() == ColumnAggregationMethod.NO_AGGREGATION)
				.map(d -> d.getColumnNameArchive()).collect(Collectors.joining(","));
		createTableInstruction.append("CREATE TABLE ");
		createTableInstruction.append(targetTableName);
		createTableInstruction.append(" AS SELECT ");
		createTableInstruction.append(sourceColumnsName);
		createTableInstruction.append(" FROM ");
		createTableInstruction.append(sourceTableName);
		createTableInstruction.append(" GROUP BY ");
		createTableInstruction.append(groupByColumnsName);
		try (Statement statement = dbConnection.createStatement()) {
			statement.execute(createTableInstruction.toString());
			dbConnection.commit();
		} catch (SQLException e) {
			throw new RuntimeException("Error creating table " + targetTableName + ": " + createTableInstruction.toString(), e);
		}
	}
	
	private void createArchiveTable(Collection<Map<String, String>> decodedFileContent) {
		StringBuilder createTableInstruction = new StringBuilder();
		createTableInstruction.append("CREATE TABLE ");
		createTableInstruction.append(ARCHIVE_TABLE_NAME);
		createTableInstruction.append("(");
		Collection<ColumnDefinition> allColumns = new HashSet<>();
		allColumns.addAll(PLANET_COLUMNS);
		allColumns.addAll(STAR_COLUMNS);
		allColumns.addAll(MISC_COLUMNS);
		for(ColumnDefinition columnDef : allColumns) {
			String sqlType;
			switch(columnDef.getColumnType().getSimpleName()) {
				case "String" :		sqlType = "VARCHAR2(250)";	break;
				case "LocalDate" :	sqlType = "DATE";			break;
				case "Integer" :	sqlType = "NUMBER";			break;
				case "BigDecimal" :	sqlType = "NUMBER";			break;
				default :			sqlType = "VARCHAR2(250)";	break;
			}
			createTableInstruction.append(columnDef.getColumnNameArchive());
			createTableInstruction.append(" ");
			createTableInstruction.append(sqlType);
				createTableInstruction.append(",\n");
		}
		createTableInstruction.delete(createTableInstruction.length()-2, createTableInstruction.length());
		createTableInstruction.append(")");
		try (Statement statement = dbConnection.createStatement()) {
			statement.execute(createTableInstruction.toString());
		} catch (SQLException e) {
			throw new RuntimeException("Error creating table " + ARCHIVE_TABLE_NAME, e);
		}
	}
}
