package org.kata.katasql.db;

final class ColumnDefinition {
	private final String columnNameArchive;
	private final String columnNameOtherTable;
	private final Class<? extends Object> columnType;
	private final ColumnAggregationMethod aggregationMethod;
	private final String columnNameAggregated;

	ColumnDefinition(String columnNameArchive, String columnNameOtherTable, Class<? extends Object> columnType, ColumnAggregationMethod aggregationMethod) {
		this.columnNameArchive = columnNameArchive;
		this.columnNameOtherTable = columnNameOtherTable;
		this.columnType = columnType;
		this.aggregationMethod = aggregationMethod;
		StringBuilder columnNameAggregatedBuilder = new StringBuilder();
		String columnBaseForErr = columnNameArchive.replace("err1", "").replace("err2", "");
		switch(aggregationMethod) {
			case NO_AGGREGATION :
				columnNameAggregatedBuilder.append(columnNameArchive);
				if(columnNameOtherTable != null && !columnNameOtherTable.equals(columnNameArchive)) {
					columnNameAggregatedBuilder.append(" ");
					columnNameAggregatedBuilder.append(columnNameOtherTable);
				}
				break;
			case KEEP_MAX :
				columnNameAggregatedBuilder.append("MAX(");
				columnNameAggregatedBuilder.append(columnNameArchive);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append(" ");
				columnNameAggregatedBuilder.append(columnNameOtherTable);
				break;
			case KEEP_MIN :
				columnNameAggregatedBuilder.append("MIN(");
				columnNameAggregatedBuilder.append(columnNameArchive);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append(" ");
				columnNameAggregatedBuilder.append(columnNameOtherTable);
				break;
			case KEEP_AVG :
				columnNameAggregatedBuilder.append("AVG(");
				columnNameAggregatedBuilder.append(columnNameArchive);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append(" ");
				columnNameAggregatedBuilder.append(columnNameOtherTable);
				break;
			case KEEP_MAX_AS_ERROR:
				columnNameAggregatedBuilder.append("MAX(");
				columnNameAggregatedBuilder.append(columnBaseForErr);
				columnNameAggregatedBuilder.append("+");
				columnNameAggregatedBuilder.append(columnNameArchive);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append("-");
				columnNameAggregatedBuilder.append("AVG(");
				columnNameAggregatedBuilder.append(columnBaseForErr);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append(" ");
				columnNameAggregatedBuilder.append(columnNameOtherTable);
				break;
			case KEEP_MIN_AS_ERROR:
				columnNameAggregatedBuilder.append("MIN(");
				columnNameAggregatedBuilder.append(columnBaseForErr);
				columnNameAggregatedBuilder.append("+");
				columnNameAggregatedBuilder.append(columnNameArchive);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append("-");
				columnNameAggregatedBuilder.append("AVG(");
				columnNameAggregatedBuilder.append(columnBaseForErr);
				columnNameAggregatedBuilder.append(")");
				columnNameAggregatedBuilder.append(" ");
				columnNameAggregatedBuilder.append(columnNameOtherTable);
				break;
		}
		columnNameAggregated = columnNameAggregatedBuilder.toString();
	}

	ColumnDefinition(String columnNameArchive, String columnNameOtherTable, Class<? extends Object> columnType) {
		this(columnNameArchive, columnNameOtherTable, columnType, ColumnAggregationMethod.NO_AGGREGATION);
	}

	ColumnDefinition(String columnNameArchive, Class<? extends Object> columnType, ColumnAggregationMethod aggregationMethod) {
		this(columnNameArchive, columnNameArchive, columnType, aggregationMethod);
	}

	ColumnDefinition(String columnNameArchive, Class<? extends Object> columnType) {
		this(columnNameArchive, columnNameArchive, columnType, ColumnAggregationMethod.NO_AGGREGATION);
	}

	public String getColumnNameArchive() {
		return columnNameArchive;
	}

	public String getColumnNameOtherTable() {
		return columnNameOtherTable;
	}

	public Class<? extends Object> getColumnType() {
		return columnType;
	}
	
	public ColumnAggregationMethod getAggregationMethod() {
		return aggregationMethod;
	}

	public String getColumnNameAggregated() {
		return columnNameAggregated;
	}

	@Override
	public int hashCode() {
		return columnNameArchive.hashCode();
	}
	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		if(!(o instanceof ColumnDefinition))
			return false;
		ColumnDefinition otherColumn = (ColumnDefinition) o;
		return columnNameArchive.equals(otherColumn.getColumnNameArchive());
	}
}
