package org.kata.katasql.file;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class CSVFileReader {
	public static final String NASA_EXOPLANET_ARCHIVE_FILE = "data/PS_2023.11.17_08.28.16.csv";
	
	private List<String> readFile(String fileName) {
		try {
			Path filePath = Paths.get(Thread.currentThread().getContextClassLoader().getResource(fileName).toURI());
			return Files.readAllLines(
					filePath,
					StandardCharsets.UTF_8);
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException("Error reading file " + fileName, e);
		}
	}
	
	public Collection<Map<String, String>> readFileAndDecode(String fileName) {
		List<String> fileContent = readFile(fileName);
		List<String> columns = null;
		Collection<Map<String, String>> decodedFileContent = new ArrayList<>();
		boolean headerRead = false;
		for(String fileLine : fileContent) {
			if(fileLine == null)
				continue;
			if(fileLine.startsWith("#"))
				continue;
			List<String> fileLineFields = Arrays.asList(fileLine.split(","));
			if(!headerRead) {
				columns = fileLineFields;
				headerRead = true;
			}
			else {
				Map<String, String> decodedLine = new HashMap<>();
				for(int i = 0; i < columns.size(); i++) {
					decodedLine.put(columns.get(i), fileLineFields.get(i));
				}
				decodedFileContent.add(decodedLine);
			}
		}
		return decodedFileContent;
	}
}
