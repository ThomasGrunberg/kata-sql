package org.kata.katasql.file;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class SQLFileReader {
	public String readQuery(String fileName) {
		try {
			Path filePath = Paths.get(Thread.currentThread().getContextClassLoader().getResource(fileName).toURI());
			return Files.readAllLines(
					filePath,
					StandardCharsets.UTF_8)
					.stream()
					.collect(Collectors.joining("\n"))
					.trim();
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException("Error reading file " + fileName, e);
		}
	}
}
