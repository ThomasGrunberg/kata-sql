package org.kata.katasql;

import org.kata.katasql.db.H2DatabaseConnector;

public class KataSQL {
	public static void main(String[] args) {
		H2DatabaseConnector.showBrowser();
	}
}
