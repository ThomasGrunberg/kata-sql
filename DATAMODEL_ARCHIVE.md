# Data model

## Archive table: KSQL_ARCHIVE

| Column | Type | Description |
|---|---|---|
| pl_name | VARCHAR2 | Planet name |
| hostname | VARCHAR2 | Host Name |
| pl_letter | VARCHAR2 | Planet Letter |
| hd_name | VARCHAR2 | HD ID |
| hip_name | VARCHAR2 | HIP ID |
| tic_id | VARCHAR2 | TIC ID |
| gaia_id | VARCHAR2 | GAIA ID |
| default_flag | VARCHAR2 | Default Parameter Set |
| sy_snum | NUMBER | Number of Stars |
| sy_pnum | NUMBER | Number of Planets |
| sy_mnum | NUMBER | Number of Moons |
| cb_flag | VARCHAR2 | Circumbinary Flag |
| discoverymethod | VARCHAR2 | Discovery Method |
| disc_year | NUMBER | Discovery Year |
| disc_refname | VARCHAR2 | Discovery Reference |
| disc_pubdate | VARCHAR2 | Discovery Publication Date |
| disc_locale | VARCHAR2 | Discovery Locale |
| disc_facility | VARCHAR2 | Discovery Facility |
| disc_telescope | VARCHAR2 | Discovery Telescope |
| disc_instrument | VARCHAR2 | Discovery Instrument |
| rv_flag | VARCHAR2 | Detected by Radial Velocity Variations |
| pul_flag | VARCHAR2 | Detected by Pulsar Timing Variations |
| ptv_flag | VARCHAR2 | Detected by Pulsation Timing Variations |
| tran_flag | VARCHAR2 | Detected by Transits |
| ast_flag | VARCHAR2 | Detected by Astrometric Variations |
| obm_flag | VARCHAR2 | Detected by Orbital Brightness Modulations |
| micro_flag | VARCHAR2 | Detected by Microlensing |
| etv_flag | VARCHAR2 | Detected by Eclipse Timing Variations |
| ima_flag | VARCHAR2 | Detected by Imaging |
| dkin_flag | VARCHAR2 | Detected by Disk Kinematics |
| soltype | VARCHAR2 | Solution Type |
| pl_controv_flag | VARCHAR2 | Controversial Flag |
| pl_refname | VARCHAR2 | Planetary Parameter Reference |
| pl_orbper | NUMBER | Orbital Period [days] |
| pl_orbpererr1 | NUMBER | Orbital Period Upper Unc. [days] |
| pl_orbpererr2 | NUMBER | Orbital Period Lower Unc. [days] |
| pl_orbperlim | VARCHAR2 | Orbital Period Limit Flag |
| pl_orbsmax | NUMBER | Orbit Semi-Major Axis [au]) |
| pl_orbsmaxerr1 | NUMBER | Orbit Semi-Major Axis Upper Unc. [au] |
| pl_orbsmaxerr2 | NUMBER | Orbit Semi-Major Axis Lower Unc. [au] |
| pl_orbsmaxlim | VARCHAR2 | Orbit Semi-Major Axis Limit Flag |
| pl_rade | NUMBER | Planet Radius [Earth Radius] |
| pl_radeerr1 | NUMBER | Planet Radius Upper Unc. [Earth Radius] |
| pl_radeerr2 | NUMBER | Planet Radius Lower Unc. [Earth Radius] |
| pl_radelim | VARCHAR2 | Planet Radius Limit Flag |
| pl_radj | NUMBER | Planet Radius [Jupiter Radius] |
| pl_radjerr1 | NUMBER | Planet Radius Upper Unc. [Jupiter Radius] |
| pl_radjerr2 | NUMBER | Planet Radius Lower Unc. [Jupiter Radius] |
| pl_radjlim | VARCHAR2 | Planet Radius Limit Flag |
| pl_masse | NUMBER | Planet Mass [Earth Mass] |
| pl_masseerr1 | NUMBER | Planet Mass [Earth Mass] Upper Unc. |
| pl_masseerr2 | NUMBER | Planet Mass [Earth Mass] Lower Unc. |
| pl_masselim | VARCHAR2 | Planet Mass [Earth Mass] Limit Flag |
| pl_massj | NUMBER | Planet Mass [Jupiter Mass] |
| pl_massjerr1 | NUMBER | Planet Mass [Jupiter Mass] Upper Unc. |
| pl_massjerr2 | NUMBER | Planet Mass [Jupiter Mass] Lower Unc. |
| pl_massjlim | VARCHAR2 | Planet Mass [Jupiter Mass] Limit Flag |
| pl_msinie | NUMBER | Planet Mass*sin(i) [Earth Mass] |
| pl_msinieerr1 | NUMBER | Planet Mass*sin(i) [Earth Mass] Upper Unc. |
| pl_msinieerr2 | NUMBER | Planet Mass*sin(i) [Earth Mass] Lower Unc. |
| pl_msinielim | VARCHAR2 | Planet Mass*sin(i) [Earth Mass] Limit Flag |
| pl_msinij | NUMBER | Planet Mass*sin(i) [Jupiter Mass] |
| pl_msinijerr1 | NUMBER | Planet Mass*sin(i) [Jupiter Mass] Upper Unc. |
| pl_msinijerr2 | NUMBER | Planet Mass*sin(i) [Jupiter Mass] Lower Unc. |
| pl_msinijlim | VARCHAR2 | Planet Mass*sin(i) [Jupiter Mass] Limit Flag |
| pl_cmasse | NUMBER | Planet Mass*sin(i)/sin(i) [Earth Mass] |
| pl_cmasseerr1 | NUMBER | Planet Mass*sin(i)/sin(i) [Earth Mass] Upper Unc. |
| pl_cmasseerr2 | NUMBER | Planet Mass*sin(i)/sin(i) [Earth Mass] Lower Unc. |
| pl_cmasselim | VARCHAR2 | Planet Mass*sin(i)/sin(i) [Earth Mass] Limit Flag |
| pl_cmassj | NUMBER | Planet Mass*sin(i)/sin(i) [Jupiter Mass] |
| pl_cmassjerr1 | NUMBER | Planet Mass*sin(i)/sin(i) [Jupiter Mass] Upper Unc. |
| pl_cmassjerr2 | NUMBER | Planet Mass*sin(i)/sin(i) [Jupiter Mass] Lower Unc. |
| pl_cmassjlim | VARCHAR2 | Planet Mass*sin(i)/sin(i) [Jupiter Mass] Limit Flag |
| pl_bmasse | NUMBER | Planet Mass or Mass*sin(i) [Earth Mass] |
| pl_bmasseerr1 | NUMBER | Planet Mass or Mass*sin(i) [Earth Mass] Upper Unc. |
| pl_bmasseerr2 | NUMBER | Planet Mass or Mass*sin(i) [Earth Mass] Lower Unc. |
| pl_bmasselim | VARCHAR2 | Planet Mass or Mass*sin(i) [Earth Mass] Limit Flag |
| pl_bmassj | NUMBER | Planet Mass or Mass*sin(i) [Jupiter Mass] |
| pl_bmassjerr1 | NUMBER | Planet Mass or Mass*sin(i) [Jupiter Mass] Upper Unc. |
| pl_bmassjerr2 | NUMBER | Planet Mass or Mass*sin(i) [Jupiter Mass] Lower Unc. |
| pl_bmassjlim | VARCHAR2 | Planet Mass or Mass*sin(i) [Jupiter Mass] Limit Flag |
| pl_bmassprov | VARCHAR2 | Planet Mass or Mass*sin(i) Provenance |
| pl_dens | NUMBER | Planet Density [g/cm**3] |
| pl_denserr1 | NUMBER | Planet Density Upper Unc. [g/cm**3] |
| pl_denserr2 | NUMBER | Planet Density Lower Unc. [g/cm**3] |
| pl_denslim | VARCHAR2 | Planet Density Limit Flag |
| pl_orbeccen | NUMBER | Eccentricity |
| pl_orbeccenerr1 | NUMBER | Eccentricity Upper Unc. |
| pl_orbeccenerr2 | NUMBER | Eccentricity Lower Unc. |
| pl_orbeccenlim | VARCHAR2 | Eccentricity Limit Flag |
| pl_insol | NUMBER | Insolation Flux [Earth Flux] |
| pl_insolerr1 | NUMBER | Insolation Flux Upper Unc. [Earth Flux] |
| pl_insolerr2 | NUMBER | Insolation Flux Lower Unc. [Earth Flux] |
| pl_insollim | VARCHAR2 | Insolation Flux Limit Flag |
| pl_eqt | NUMBER | Equilibrium Temperature [K] |
| pl_eqterr1 | NUMBER | Equilibrium Temperature Upper Unc. [K] |
| pl_eqterr2 | NUMBER | Equilibrium Temperature Lower Unc. [K] |
| pl_eqtlim | VARCHAR2 | Equilibrium Temperature Limit Flag |
| pl_orbincl | NUMBER | Inclination [deg] |
| pl_orbinclerr1 | NUMBER | Inclination Upper Unc. [deg] |
| pl_orbinclerr2 | NUMBER | Inclination Lower Unc. [deg] |
| pl_orbincllim | VARCHAR2 | Inclination Limit Flag |
| pl_tranmid | NUMBER | Transit Midpoint [days] |
| pl_tranmiderr1 | NUMBER | Transit Midpoint Upper Unc. [days] |
| pl_tranmiderr2 | NUMBER | Transit Midpoint Lower Unc. [days] |
| pl_tranmidlim | VARCHAR2 | Transit Midpoint Limit Flag |
| pl_tsystemref | VARCHAR2 | Time Reference Frame and Standard |
| ttv_flag | VARCHAR2 | Data show Transit Timing Variations |
| pl_imppar | VARCHAR2 | Impact Parameter |
| pl_impparerr1 | VARCHAR2 | Impact Parameter Upper Unc. |
| pl_impparerr2 | VARCHAR2 | Impact Parameter Lower Unc. |
| pl_impparlim | VARCHAR2 | Impact Parameter Limit Flag |
| pl_trandep | NUMBER | Transit Depth [%] |
| pl_trandeperr1 | NUMBER | Transit Depth Upper Unc. [%] |
| pl_trandeperr2 | NUMBER | Transit Depth Lower Unc. [%] |
| pl_trandeplim | VARCHAR2 | Transit Depth Limit Flag |
| pl_trandur | NUMBER | Transit Duration [hours] |
| pl_trandurerr1 | NUMBER | Transit Duration Upper Unc. [hours] |
| pl_trandurerr2 | NUMBER | Transit Duration Lower Unc. [hours] |
| pl_trandurlim | VARCHAR2 | Transit Duration Limit Flag |
| pl_ratdor | NUMBER | Ratio of Semi-Major Axis to Stellar Radius |
| pl_ratdorerr1 | NUMBER | Ratio of Semi-Major Axis to Stellar Radius Upper Unc. |
| pl_ratdorerr2 | NUMBER | Ratio of Semi-Major Axis to Stellar Radius Lower Unc. |
| pl_ratdorlim | VARCHAR2 | Ratio of Semi-Major Axis to Stellar Radius Limit Flag |
| pl_ratror | NUMBER | Ratio of Planet to Stellar Radius |
| pl_ratrorerr1 | NUMBER | Ratio of Planet to Stellar Radius Upper Unc. |
| pl_ratrorerr2 | NUMBER | Ratio of Planet to Stellar Radius Lower Unc. |
| pl_ratrorlim | VARCHAR2 | Ratio of Planet to Stellar Radius Limit Flag |
| pl_occdep | NUMBER | Occultation Depth [%] |
| pl_occdeperr1 | NUMBER | Occultation Depth Upper Unc. [%] |
| pl_occdeperr2 | NUMBER | Occultation Depth Lower Unc. [%] |
| pl_occdeplim | VARCHAR2 | Occultation Depth Limit Flag |
| pl_orbtper | NUMBER | Epoch of Periastron [days] |
| pl_orbtpererr1 | NUMBER | Epoch of Periastron Upper Unc. [days] |
| pl_orbtpererr2 | NUMBER | Epoch of Periastron Lower Unc. [days] |
| pl_orbtperlim | VARCHAR2 | Epoch of Periastron Limit Flag |
| pl_orblper | VARCHAR2 | Argument of Periastron [deg] |
| pl_orblpererr1 | VARCHAR2 | Argument of Periastron Upper Unc. [deg] |
| pl_orblpererr2 | VARCHAR2 | Argument of Periastron Lower Unc. [deg] |
| pl_orblperlim | VARCHAR2 | Argument of Periastron Limit Flag |
| pl_rvamp | NUMBER | Radial Velocity Amplitude [m/s] |
| pl_rvamperr1 | NUMBER | Radial Velocity Amplitude Upper Unc. [m/s] |
| pl_rvamperr2 | NUMBER | Radial Velocity Amplitude Lower Unc. [m/s] |
| pl_rvamplim | VARCHAR2 | Radial Velocity Amplitude Limit Flag |
| pl_projobliq | NUMBER | Projected Obliquity [deg] |
| pl_projobliqerr1 | NUMBER | Projected Obliquity Upper Unc. [deg] |
| pl_projobliqerr2 | NUMBER | Projected Obliquity Lower Unc. [deg] |
| pl_projobliqlim | VARCHAR2 | Projected Obliquity Limit Flag |
| pl_trueobliq | NUMBER | True Obliquity [deg] |
| pl_trueobliqerr1 | NUMBER | True Obliquity Upper Unc. [deg] |
| pl_trueobliqerr2 | NUMBER | True Obliquity Lower Unc. [deg] |
| pl_trueobliqlim | VARCHAR2 | True Obliquity Limit Flag |
| st_refname | VARCHAR2 | Stellar Parameter Reference |
| st_spectype | VARCHAR2 | Spectral Type |
| st_teff | NUMBER | Stellar Effective Temperature [K] |
| st_tefferr1 | NUMBER | Stellar Effective Temperature Upper Unc. [K] |
| st_tefferr2 | NUMBER | Stellar Effective Temperature Lower Unc. [K] |
| st_tefflim | VARCHAR2 | Stellar Effective Temperature Limit Flag |
| st_rad | NUMBER | Stellar Radius [Solar Radius] |
| st_raderr1 | NUMBER | Stellar Radius Upper Unc. [Solar Radius] |
| st_raderr2 | NUMBER | Stellar Radius Lower Unc. [Solar Radius] |
| st_radlim | VARCHAR2 | Stellar Radius Limit Flag |
| st_mass | NUMBER | Stellar Mass [Solar mass] |
| st_masserr1 | NUMBER | Stellar Mass Upper Unc. [Solar mass] |
| st_masserr2 | NUMBER | Stellar Mass Lower Unc. [Solar mass] |
| st_masslim | VARCHAR2 | Stellar Mass Limit Flag |
| st_met | NUMBER | Stellar Metallicity [dex] |
| st_meterr1 | NUMBER | Stellar Metallicity Upper Unc. [dex] |
| st_meterr2 | NUMBER | Stellar Metallicity Lower Unc. [dex] |
| st_metlim | VARCHAR2 | Stellar Metallicity Limit Flag |
| st_metratio | NUMBER | Stellar Metallicity Ratio |
| st_lum | NUMBER | Stellar Luminosity [log(Solar)] |
| st_lumerr1 | NUMBER | Stellar Luminosity Upper Unc. [log(Solar)] |
| st_lumerr2 | NUMBER | Stellar Luminosity Lower Unc. [log(Solar)] |
| st_lumlim | VARCHAR2 | Stellar Luminosity Limit Flag |
| st_logg | NUMBER | Stellar Surface Gravity [log10(cm/s**2)] |
| st_loggerr1 | NUMBER | Stellar Surface Gravity Upper Unc. [log10(cm/s**2)] |
| st_loggerr2 | NUMBER | Stellar Surface Gravity Lower Unc. [log10(cm/s**2)] |
| st_logglim | VARCHAR2 | Stellar Surface Gravity Limit Flag |
| st_age | NUMBER | Stellar Age [Gyr] |
| st_ageerr1 | NUMBER | Stellar Age Upper Unc. [Gyr] |
| st_ageerr2 | NUMBER | Stellar Age Lower Unc. [Gyr] |
| st_agelim | VARCHAR2 | Stellar Age Limit Flag |
| st_dens | NUMBER | Stellar Density [g/cm**3] |
| st_denserr1 | NUMBER | Stellar Density Upper Unc. [g/cm**3] |
| st_denserr2 | NUMBER | Stellar Density Lower Unc. [g/cm**3] |
| st_denslim | VARCHAR2 | Stellar Density Limit Flag |
| st_vsin | NUMBER | Stellar Rotational Velocity [km/s] |
| st_vsinerr1 | NUMBER | Stellar Rotational Velocity [km/s] Upper Unc. |
| st_vsinerr2 | NUMBER | Stellar Rotational Velocity [km/s] Lower Unc. |
| st_vsinlim | VARCHAR2 | Stellar Rotational Velocity Limit Flag |
| st_rotp | NUMBER | Stellar Rotational Period [days] |
| st_rotperr1 | NUMBER | Stellar Rotational Period [days] Upper Unc. |
| st_rotperr2 | NUMBER | Stellar Rotational Period [days] Lower Unc. |
| st_rotplim | VARCHAR2 | Stellar Rotational Period Limit Flag |
| st_radv | NUMBER | Systemic Radial Velocity [km/s] |
| st_radverr1 | NUMBER | Systemic Radial Velocity Upper Unc. [km/s] |
| st_radverr2 | NUMBER | Systemic Radial Velocity Lower Unc. [km/s] |
| st_radvlim | VARCHAR2 | Systemic Radial Velocity Limit Flag |
| sy_refname | VARCHAR2 | System Parameter Reference |
| rastr | VARCHAR2 | RA [sexagesimal] |
| ra | NUMBER | RA [deg] |
| decstr | VARCHAR2 | Dec [sexagesimal] |
| dec | NUMBER | Dec [deg] |
| glat | NUMBER | Galactic Latitude [deg] |
| glon | NUMBER | Galactic Longitude [deg] |
| elat | NUMBER | Ecliptic Latitude [deg] |
| elon | NUMBER | Ecliptic Longitude [deg] |
| sy_pm | VARCHAR2 | Total Proper Motion [mas/yr] |
| sy_pmerr1 | VARCHAR2 | Total Proper Motion Upper Unc [mas/yr] |
| sy_pmerr2 | VARCHAR2 | Total Proper Motion Lower Unc [mas/yr] |
| sy_pmra | VARCHAR2 | Proper Motion (RA) [mas/yr] |
| sy_pmraerr1 | VARCHAR2 | Proper Motion (RA) [mas/yr] Upper Unc |
| sy_pmraerr2 | VARCHAR2 | Proper Motion (RA) [mas/yr] Lower Unc |
| sy_pmdec | VARCHAR2 | Proper Motion (Dec) [mas/yr] |
| sy_pmdecerr1 | VARCHAR2 | Proper Motion (Dec) [mas/yr] Upper Unc |
| sy_pmdecerr2 | VARCHAR2 | Proper Motion (Dec) [mas/yr] Lower Unc |
| sy_dist | NUMBER | Distance [pc] |
| sy_disterr1 | NUMBER | Distance [pc] Upper Unc |
| sy_disterr2 | NUMBER | Distance [pc] Lower Unc |
| sy_plx | VARCHAR2 | Parallax [mas] |
| sy_plxerr1 | VARCHAR2 | Parallax [mas] Upper Unc |
| sy_plxerr2 | VARCHAR2 | Parallax [mas] Lower Unc |
| sy_bmag | VARCHAR2 | B (Johnson) Magnitude |
| sy_bmagerr1 | VARCHAR2 | B (Johnson) Magnitude Upper Unc |
| sy_bmagerr2 | VARCHAR2 | B (Johnson) Magnitude Lower Unc |
| sy_vmag | VARCHAR2 | V (Johnson) Magnitude |
| sy_vmagerr1 | VARCHAR2 | V (Johnson) Magnitude Upper Unc |
| sy_vmagerr2 | VARCHAR2 | V (Johnson) Magnitude Lower Unc |
| sy_jmag | VARCHAR2 | J (2MASS) Magnitude |
| sy_jmagerr1 | VARCHAR2 | J (2MASS) Magnitude Upper Unc |
| sy_jmagerr2 | VARCHAR2 | J (2MASS) Magnitude Lower Unc |
| sy_hmag | VARCHAR2 | H (2MASS) Magnitude |
| sy_hmagerr1 | VARCHAR2 | H (2MASS) Magnitude Upper Unc |
| sy_hmagerr2 | VARCHAR2 | H (2MASS) Magnitude Lower Unc |
| sy_kmag | VARCHAR2 | Ks (2MASS) Magnitude |
| sy_kmagerr1 | VARCHAR2 | Ks (2MASS) Magnitude Upper Unc |
| sy_kmagerr2 | VARCHAR2 | Ks (2MASS) Magnitude Lower Unc |
| sy_umag | VARCHAR2 | u (Sloan) Magnitude |
| sy_umagerr1 | VARCHAR2 | u (Sloan) Magnitude Upper Unc |
| sy_umagerr2 | VARCHAR2 | u (Sloan) Magnitude Lower Unc |
| sy_gmag | VARCHAR2 | g (Sloan) Magnitude |
| sy_gmagerr1 | VARCHAR2 | g (Sloan) Magnitude Upper Unc |
| sy_gmagerr2 | VARCHAR2 | g (Sloan) Magnitude Lower Unc |
| sy_rmag | VARCHAR2 | r (Sloan) Magnitude |
| sy_rmagerr1 | VARCHAR2 | r (Sloan) Magnitude Upper Unc |
| sy_rmagerr2 | VARCHAR2 | r (Sloan) Magnitude Lower Unc |
| sy_imag | VARCHAR2 | i (Sloan) Magnitude |
| sy_imagerr1 | VARCHAR2 | i (Sloan) Magnitude Upper Unc |
| sy_imagerr2 | VARCHAR2 | i (Sloan) Magnitude Lower Unc |
| sy_zmag | VARCHAR2 | z (Sloan) Magnitude |
| sy_zmagerr1 | VARCHAR2 | z (Sloan) Magnitude Upper Unc |
| sy_zmagerr2 | VARCHAR2 | z (Sloan) Magnitude Lower Unc |
| sy_w1mag | VARCHAR2 | W1 (WISE) Magnitude |
| sy_w1magerr1 | VARCHAR2 | W1 (WISE) Magnitude Upper Unc |
| sy_w1magerr2 | VARCHAR2 | W1 (WISE) Magnitude Lower Unc |
| sy_w2mag | VARCHAR2 | W2 (WISE) Magnitude |
| sy_w2magerr1 | VARCHAR2 | W2 (WISE) Magnitude Upper Unc |
| sy_w2magerr2 | VARCHAR2 | W2 (WISE) Magnitude Lower Unc |
| sy_w3mag | VARCHAR2 | W3 (WISE) Magnitude |
| sy_w3magerr1 | VARCHAR2 | W3 (WISE) Magnitude Upper Unc |
| sy_w3magerr2 | VARCHAR2 | W3 (WISE) Magnitude Lower Unc |
| sy_w4mag | VARCHAR2 | W4 (WISE) Magnitude |
| sy_w4magerr1 | VARCHAR2 | W4 (WISE) Magnitude Upper Unc |
| sy_w4magerr2 | VARCHAR2 | W4 (WISE) Magnitude Lower Unc |
| sy_gaiamag | VARCHAR2 | Gaia Magnitude |
| sy_gaiamagerr1 | VARCHAR2 | Gaia Magnitude Upper Unc |
| sy_gaiamagerr2 | VARCHAR2 | Gaia Magnitude Lower Unc |
| sy_icmag | VARCHAR2 | I (Cousins) Magnitude |
| sy_icmagerr1 | VARCHAR2 | I (Cousins) Magnitude Upper Unc |
| sy_icmagerr2 | VARCHAR2 | I (Cousins) Magnitude Lower Unc |
| sy_tmag | VARCHAR2 | TESS Magnitude |
| sy_tmagerr1 | VARCHAR2 | TESS Magnitude Upper Unc |
| sy_tmagerr2 | VARCHAR2 | TESS Magnitude Lower Unc |
| sy_kepmag | VARCHAR2 | Kepler Magnitude |
| sy_kepmagerr1 | VARCHAR2 | Kepler Magnitude Upper Unc |
| sy_kepmagerr2 | VARCHAR2 | Kepler Magnitude Lower Unc |
| rowupdate | VARCHAR2 | Date of Last Update |
| pl_pubdate | VARCHAR2 | Planetary Parameter Reference Publication Date |
| releasedate | VARCHAR2 | Release Date |
| pl_nnotes | VARCHAR2 | Number of Notes |
| st_nphot | VARCHAR2 | Number of Photometry Time Series |
| st_nrvc | VARCHAR2 | Number of Radial Velocity Time Series |
| st_nspec | VARCHAR2 | Number of Stellar Spectra Measurements |
| pl_nespec | VARCHAR2 | Number of Emission Spectroscopy Measurements |
| pl_ntranspec | VARCHAR2 | Number of Transmission Spectroscopy Measurements |
