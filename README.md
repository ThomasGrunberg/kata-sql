# Kata SQL
The NASA exoplanet science institute provides a detailed list of all known exoplanets, with a vast amount of related information: planet name, discovery method, known weight and size, etc.

More details can be found here: https://exoplanetarchive.ipac.caltech.edu/

We want to have a better understanding of our place in the universe by exploring this dataspace.

Note: This kata uses an extraction of the archive from november 2023. It was correct at the time, but for the purpose of this kata, the archive will remain frozen and will not take into account more recent discoveries. As such, the information is probably already outdated as you read these lines.

## Requirements
In order to do this kata, you will need the following:
- Maven 3 or newer
- JDK 11 or newer

## How to fill the kata
For each exercise below, an empty .sql file exists in the directory src/test/resources/exercises. Fill it with a query and run the project's maven build command 'mvn test'. Alternatively, create a develop branch, fill the .sql file and push the commit so the CI/CD pipe runs.

## The database
The database's datamodel can be found [here](DATAMODEL.md). It can also be queried by running the application, using the following command: 'mvn compile exec:java'. This will open a window in your browser.
Note: Because this command keeps the database alive as long as is needed, it will only terminate when the browser window is disconnected, using the red 'disconnect' button on the top left. If the browser window is closed in any other way, the maven command will have to be terminated.

## The Kata
### Exercise 1: How many exoplanets are known?
Find out the number of exoplanets in this extraction. The NASA's archive is particularly verbose and detailed, and contains redundant information for some discovered exoplanets. For example, the exoplanet Proxima Centauri b is present in the table with four different lines, each one containing identical information for the exoplanet, but differing information regarding its discovery.

Note: For now, only use the table KSQL_ARCHIVE, a database image of the Nasa data archive. Its model is described in the [data model](DATAMODEL.md).

### Exercise 2: Let's split the table.
That archive table is making things unnecessarily complicated. We need to split its data into relevant tables for easier querying.
Create two tables containing respectively exoplanet and star data. Name them KSQL_EXOPL and KSQL_EXOSTARS ; Only include the bare minimum information of each type of object for now: planet name and star name for the first, star name and spectral type for the second.

### Exercise 3: How many exoplanets are there around stars like ours
Our star is not unique in the universe. It's a yellow dwarf, fairly common star in the universe. If we want to look for a new home, or if we're looking for extraterrestrial life, it makes sense to look for something similar to what we know.
Of the many exoplanets known, how many are around a yellow dwarf like ours (st_spectype G2 V)?
Don't use the KSQL_ARCHIVE table. Use the tables KSQL_EXOPLANETS and KSQL_STARS described in the [data model](DATAMODEL.md).

### Exercise 4: How many exoplanets have a terrestrial mass
This was not helpful. Far too many of these exoplanets are uninteresting gas giants where life (as we know it) cannot exist. We should focus our attention on planets similar to ours instead.
Let's focus on mass for now. How many exoplanets, within the known uncertaintity, could have a mass similar to Earth's?
Note: It is recommanded to use the KSQL_EXOPLANETS.
