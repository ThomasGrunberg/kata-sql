# Data model

## Stars table: KSQL_STARS

| Column | Type | Description |
|---|---|---|
| st_name | VARCHAR2 | Host Name |
| st_refname | VARCHAR2 | Stellar Parameter Reference |
| st_spectype | VARCHAR2 | Spectral Type |
| st_teff | NUMBER | Stellar Effective Temperature [K] |
| st_tefferr1 | NUMBER | Stellar Effective Temperature Upper Unc. [K] |
| st_tefferr2 | NUMBER | Stellar Effective Temperature Lower Unc. [K] |
| st_tefflim | VARCHAR2 | Stellar Effective Temperature Limit Flag |
| st_rad | NUMBER | Stellar Radius [Solar Radius] |
| st_raderr1 | NUMBER | Stellar Radius Upper Unc. [Solar Radius] |
| st_raderr2 | NUMBER | Stellar Radius Lower Unc. [Solar Radius] |
| st_radlim | VARCHAR2 | Stellar Radius Limit Flag |
| st_mass | NUMBER | Stellar Mass [Solar mass] |
| st_masserr1 | NUMBER | Stellar Mass Upper Unc. [Solar mass] |
| st_masserr2 | NUMBER | Stellar Mass Lower Unc. [Solar mass] |
| st_masslim | VARCHAR2 | Stellar Mass Limit Flag |
| st_met | NUMBER | Stellar Metallicity [dex] |
| st_meterr1 | NUMBER | Stellar Metallicity Upper Unc. [dex] |
| st_meterr2 | NUMBER | Stellar Metallicity Lower Unc. [dex] |
| st_metlim | VARCHAR2 | Stellar Metallicity Limit Flag |
| st_metratio | NUMBER | Stellar Metallicity Ratio |
| st_lum | NUMBER | Stellar Luminosity [log(Solar)] |
| st_lumerr1 | NUMBER | Stellar Luminosity Upper Unc. [log(Solar)] |
| st_lumerr2 | NUMBER | Stellar Luminosity Lower Unc. [log(Solar)] |
| st_lumlim | VARCHAR2 | Stellar Luminosity Limit Flag |
| st_logg | NUMBER | Stellar Surface Gravity [log10(cm/s**2)] |
| st_loggerr1 | NUMBER | Stellar Surface Gravity Upper Unc. [log10(cm/s**2)] |
| st_loggerr2 | NUMBER | Stellar Surface Gravity Lower Unc. [log10(cm/s**2)] |
| st_logglim | VARCHAR2 | Stellar Surface Gravity Limit Flag |
| st_age | NUMBER | Stellar Age [Gyr] |
| st_ageerr1 | NUMBER | Stellar Age Upper Unc. [Gyr] |
| st_ageerr2 | NUMBER | Stellar Age Lower Unc. [Gyr] |
| st_agelim | VARCHAR2 | Stellar Age Limit Flag |
| st_dens | NUMBER | Stellar Density [g/cm**3] |
| st_denserr1 | NUMBER | Stellar Density Upper Unc. [g/cm**3] |
| st_denserr2 | NUMBER | Stellar Density Lower Unc. [g/cm**3] |
| st_denslim | VARCHAR2 | Stellar Density Limit Flag |
| st_vsin | NUMBER | Stellar Rotational Velocity [km/s] |
| st_vsinerr1 | NUMBER | Stellar Rotational Velocity [km/s] Upper Unc. |
| st_vsinerr2 | NUMBER | Stellar Rotational Velocity [km/s] Lower Unc. |
| st_vsinlim | VARCHAR2 | Stellar Rotational Velocity Limit Flag |
| st_rotp | NUMBER | Stellar Rotational Period [days] |
| st_rotperr1 | NUMBER | Stellar Rotational Period [days] Upper Unc. |
| st_rotperr2 | NUMBER | Stellar Rotational Period [days] Lower Unc. |
| st_rotplim | VARCHAR2 | Stellar Rotational Period Limit Flag |
| st_radv | NUMBER | Systemic Radial Velocity [km/s] |
| st_radverr1 | NUMBER | Systemic Radial Velocity Upper Unc. [km/s] |
| st_radverr2 | NUMBER | Systemic Radial Velocity Lower Unc. [km/s] |
| st_radvlim | VARCHAR2 | Systemic Radial Velocity Limit Flag |
| sy_refname | VARCHAR2 | System Parameter Reference |
| rastr | VARCHAR2 | RA [sexagesimal] |
| ra | NUMBER | RA [deg] |
| decstr | VARCHAR2 | Dec [sexagesimal] |
| dec | NUMBER | Dec [deg] |
| glat | NUMBER | Galactic Latitude [deg] |
| glon | NUMBER | Galactic Longitude [deg] |
| elat | NUMBER | Ecliptic Latitude [deg] |
| elon | NUMBER | Ecliptic Longitude [deg] |
| sy_pm | VARCHAR2 | Total Proper Motion [mas/yr] |
| sy_pmerr1 | VARCHAR2 | Total Proper Motion Upper Unc [mas/yr] |
| sy_pmerr2 | VARCHAR2 | Total Proper Motion Lower Unc [mas/yr] |
| sy_pmra | VARCHAR2 | Proper Motion (RA) [mas/yr] |
| sy_pmraerr1 | VARCHAR2 | Proper Motion (RA) [mas/yr] Upper Unc |
| sy_pmraerr2 | VARCHAR2 | Proper Motion (RA) [mas/yr] Lower Unc |
| sy_pmdec | VARCHAR2 | Proper Motion (Dec) [mas/yr] |
| sy_pmdecerr1 | VARCHAR2 | Proper Motion (Dec) [mas/yr] Upper Unc |
| sy_pmdecerr2 | VARCHAR2 | Proper Motion (Dec) [mas/yr] Lower Unc |
| sy_dist | NUMBER | Distance [pc] |
| sy_disterr1 | NUMBER | Distance [pc] Upper Unc |
| sy_disterr2 | NUMBER | Distance [pc] Lower Unc |
| sy_plx | VARCHAR2 | Parallax [mas] |
| sy_plxerr1 | VARCHAR2 | Parallax [mas] Upper Unc |
| sy_plxerr2 | VARCHAR2 | Parallax [mas] Lower Unc |
| sy_bmag | VARCHAR2 | B (Johnson) Magnitude |
| sy_bmagerr1 | VARCHAR2 | B (Johnson) Magnitude Upper Unc |
| sy_bmagerr2 | VARCHAR2 | B (Johnson) Magnitude Lower Unc |
| sy_vmag | VARCHAR2 | V (Johnson) Magnitude |
| sy_vmagerr1 | VARCHAR2 | V (Johnson) Magnitude Upper Unc |
| sy_vmagerr2 | VARCHAR2 | V (Johnson) Magnitude Lower Unc |
| sy_jmag | VARCHAR2 | J (2MASS) Magnitude |
| sy_jmagerr1 | VARCHAR2 | J (2MASS) Magnitude Upper Unc |
| sy_jmagerr2 | VARCHAR2 | J (2MASS) Magnitude Lower Unc |
| sy_hmag | VARCHAR2 | H (2MASS) Magnitude |
| sy_hmagerr1 | VARCHAR2 | H (2MASS) Magnitude Upper Unc |
| sy_hmagerr2 | VARCHAR2 | H (2MASS) Magnitude Lower Unc |
| sy_kmag | VARCHAR2 | Ks (2MASS) Magnitude |
| sy_kmagerr1 | VARCHAR2 | Ks (2MASS) Magnitude Upper Unc |
| sy_kmagerr2 | VARCHAR2 | Ks (2MASS) Magnitude Lower Unc |
| sy_umag | VARCHAR2 | u (Sloan) Magnitude |
| sy_umagerr1 | VARCHAR2 | u (Sloan) Magnitude Upper Unc |
| sy_umagerr2 | VARCHAR2 | u (Sloan) Magnitude Lower Unc |
| sy_gmag | VARCHAR2 | g (Sloan) Magnitude |
| sy_gmagerr1 | VARCHAR2 | g (Sloan) Magnitude Upper Unc |
| sy_gmagerr2 | VARCHAR2 | g (Sloan) Magnitude Lower Unc |
| sy_rmag | VARCHAR2 | r (Sloan) Magnitude |
| sy_rmagerr1 | VARCHAR2 | r (Sloan) Magnitude Upper Unc |
| sy_rmagerr2 | VARCHAR2 | r (Sloan) Magnitude Lower Unc |
| sy_imag | VARCHAR2 | i (Sloan) Magnitude |
| sy_imagerr1 | VARCHAR2 | i (Sloan) Magnitude Upper Unc |
| sy_imagerr2 | VARCHAR2 | i (Sloan) Magnitude Lower Unc |
| sy_zmag | VARCHAR2 | z (Sloan) Magnitude |
| sy_zmagerr1 | VARCHAR2 | z (Sloan) Magnitude Upper Unc |
| sy_zmagerr2 | VARCHAR2 | z (Sloan) Magnitude Lower Unc |
| sy_w1mag | VARCHAR2 | W1 (WISE) Magnitude |
| sy_w1magerr1 | VARCHAR2 | W1 (WISE) Magnitude Upper Unc |
| sy_w1magerr2 | VARCHAR2 | W1 (WISE) Magnitude Lower Unc |
| sy_w2mag | VARCHAR2 | W2 (WISE) Magnitude |
| sy_w2magerr1 | VARCHAR2 | W2 (WISE) Magnitude Upper Unc |
| sy_w2magerr2 | VARCHAR2 | W2 (WISE) Magnitude Lower Unc |
| sy_w3mag | VARCHAR2 | W3 (WISE) Magnitude |
| sy_w3magerr1 | VARCHAR2 | W3 (WISE) Magnitude Upper Unc |
| sy_w3magerr2 | VARCHAR2 | W3 (WISE) Magnitude Lower Unc |
| sy_w4mag | VARCHAR2 | W4 (WISE) Magnitude |
| sy_w4magerr1 | VARCHAR2 | W4 (WISE) Magnitude Upper Unc |
| sy_w4magerr2 | VARCHAR2 | W4 (WISE) Magnitude Lower Unc |
| sy_gaiamag | VARCHAR2 | Gaia Magnitude |
| sy_gaiamagerr1 | VARCHAR2 | Gaia Magnitude Upper Unc |
| sy_gaiamagerr2 | VARCHAR2 | Gaia Magnitude Lower Unc |
| sy_icmag | VARCHAR2 | I (Cousins) Magnitude |
| sy_icmagerr1 | VARCHAR2 | I (Cousins) Magnitude Upper Unc |
| sy_icmagerr2 | VARCHAR2 | I (Cousins) Magnitude Lower Unc |
| sy_tmag | VARCHAR2 | TESS Magnitude |
| sy_tmagerr1 | VARCHAR2 | TESS Magnitude Upper Unc |
| sy_tmagerr2 | VARCHAR2 | TESS Magnitude Lower Unc |
| sy_kepmag | VARCHAR2 | Kepler Magnitude |
| sy_kepmagerr1 | VARCHAR2 | Kepler Magnitude Upper Unc |
| sy_kepmagerr2 | VARCHAR2 | Kepler Magnitude Lower Unc |
