# Data model

## Planets table: KSQL_EXOPLANETS

| Column | Type | Description |
|---|---|---|
| pl_name | VARCHAR2 | Planet name |
| st_name | VARCHAR2 | Host Name |
| pl_letter | VARCHAR2 | Planet Letter |
| pl_controv_flag | VARCHAR2 | Controversial Flag |
| pl_refname | VARCHAR2 | Planetary Parameter Reference |
| pl_orbper | NUMBER | Orbital Period [days] |
| pl_orbpererr1 | NUMBER | Orbital Period Upper Unc. [days] |
| pl_orbpererr2 | NUMBER | Orbital Period Lower Unc. [days] |
| pl_orbperlim | VARCHAR2 | Orbital Period Limit Flag |
| pl_orbsmax | NUMBER | Orbit Semi-Major Axis [au]) |
| pl_orbsmaxerr1 | NUMBER | Orbit Semi-Major Axis Upper Unc. [au] |
| pl_orbsmaxerr2 | NUMBER | Orbit Semi-Major Axis Lower Unc. [au] |
| pl_orbsmaxlim | VARCHAR2 | Orbit Semi-Major Axis Limit Flag |
| pl_rade | NUMBER | Planet Radius [Earth Radius] |
| pl_radeerr1 | NUMBER | Planet Radius Upper Unc. [Earth Radius] |
| pl_radeerr2 | NUMBER | Planet Radius Lower Unc. [Earth Radius] |
| pl_radelim | VARCHAR2 | Planet Radius Limit Flag |
| pl_radj | NUMBER | Planet Radius [Jupiter Radius] |
| pl_radjerr1 | NUMBER | Planet Radius Upper Unc. [Jupiter Radius] |
| pl_radjerr2 | NUMBER | Planet Radius Lower Unc. [Jupiter Radius] |
| pl_radjlim | VARCHAR2 | Planet Radius Limit Flag |
| pl_masse | NUMBER | Planet Mass [Earth Mass] |
| pl_masseerr1 | NUMBER | Planet Mass [Earth Mass] Upper Unc. |
| pl_masseerr2 | NUMBER | Planet Mass [Earth Mass] Lower Unc. |
| pl_masselim | VARCHAR2 | Planet Mass [Earth Mass] Limit Flag |
| pl_massj | NUMBER | Planet Mass [Jupiter Mass] |
| pl_massjerr1 | NUMBER | Planet Mass [Jupiter Mass] Upper Unc. |
| pl_massjerr2 | NUMBER | Planet Mass [Jupiter Mass] Lower Unc. |
| pl_massjlim | VARCHAR2 | Planet Mass [Jupiter Mass] Limit Flag |
| pl_msinie | NUMBER | Planet Mass*sin(i) [Earth Mass] |
| pl_msinieerr1 | NUMBER | Planet Mass*sin(i) [Earth Mass] Upper Unc. |
| pl_msinieerr2 | NUMBER | Planet Mass*sin(i) [Earth Mass] Lower Unc. |
| pl_msinielim | VARCHAR2 | Planet Mass*sin(i) [Earth Mass] Limit Flag |
| pl_msinij | NUMBER | Planet Mass*sin(i) [Jupiter Mass] |
| pl_msinijerr1 | NUMBER | Planet Mass*sin(i) [Jupiter Mass] Upper Unc. |
| pl_msinijerr2 | NUMBER | Planet Mass*sin(i) [Jupiter Mass] Lower Unc. |
| pl_msinijlim | VARCHAR2 | Planet Mass*sin(i) [Jupiter Mass] Limit Flag |
| pl_cmasse | NUMBER | Planet Mass*sin(i)/sin(i) [Earth Mass] |
| pl_cmasseerr1 | NUMBER | Planet Mass*sin(i)/sin(i) [Earth Mass] Upper Unc. |
| pl_cmasseerr2 | NUMBER | Planet Mass*sin(i)/sin(i) [Earth Mass] Lower Unc. |
| pl_cmasselim | VARCHAR2 | Planet Mass*sin(i)/sin(i) [Earth Mass] Limit Flag |
| pl_cmassj | NUMBER | Planet Mass*sin(i)/sin(i) [Jupiter Mass] |
| pl_cmassjerr1 | NUMBER | Planet Mass*sin(i)/sin(i) [Jupiter Mass] Upper Unc. |
| pl_cmassjerr2 | NUMBER | Planet Mass*sin(i)/sin(i) [Jupiter Mass] Lower Unc. |
| pl_cmassjlim | VARCHAR2 | Planet Mass*sin(i)/sin(i) [Jupiter Mass] Limit Flag |
| pl_bmasse | NUMBER | Planet Mass or Mass*sin(i) [Earth Mass] |
| pl_bmasseerr1 | NUMBER | Planet Mass or Mass*sin(i) [Earth Mass] Upper Unc. |
| pl_bmasseerr2 | NUMBER | Planet Mass or Mass*sin(i) [Earth Mass] Lower Unc. |
| pl_bmasselim | VARCHAR2 | Planet Mass or Mass*sin(i) [Earth Mass] Limit Flag |
| pl_bmassj | NUMBER | Planet Mass or Mass*sin(i) [Jupiter Mass] |
| pl_bmassjerr1 | NUMBER | Planet Mass or Mass*sin(i) [Jupiter Mass] Upper Unc. |
| pl_bmassjerr2 | NUMBER | Planet Mass or Mass*sin(i) [Jupiter Mass] Lower Unc. |
| pl_bmassjlim | VARCHAR2 | Planet Mass or Mass*sin(i) [Jupiter Mass] Limit Flag |
| pl_bmassprov | VARCHAR2 | Planet Mass or Mass*sin(i) Provenance |
| pl_dens | NUMBER | Planet Density [g/cm**3] |
| pl_denserr1 | NUMBER | Planet Density Upper Unc. [g/cm**3] |
| pl_denserr2 | NUMBER | Planet Density Lower Unc. [g/cm**3] |
| pl_denslim | VARCHAR2 | Planet Density Limit Flag |
| pl_orbeccen | NUMBER | Eccentricity |
| pl_orbeccenerr1 | NUMBER | Eccentricity Upper Unc. |
| pl_orbeccenerr2 | NUMBER | Eccentricity Lower Unc. |
| pl_orbeccenlim | VARCHAR2 | Eccentricity Limit Flag |
| pl_insol | NUMBER | Insolation Flux [Earth Flux] |
| pl_insolerr1 | NUMBER | Insolation Flux Upper Unc. [Earth Flux] |
| pl_insolerr2 | NUMBER | Insolation Flux Lower Unc. [Earth Flux] |
| pl_insollim | VARCHAR2 | Insolation Flux Limit Flag |
| pl_eqt | NUMBER | Equilibrium Temperature [K] |
| pl_eqterr1 | NUMBER | Equilibrium Temperature Upper Unc. [K] |
| pl_eqterr2 | NUMBER | Equilibrium Temperature Lower Unc. [K] |
| pl_eqtlim | VARCHAR2 | Equilibrium Temperature Limit Flag |
| pl_orbincl | NUMBER | Inclination [deg] |
| pl_orbinclerr1 | NUMBER | Inclination Upper Unc. [deg] |
| pl_orbinclerr2 | NUMBER | Inclination Lower Unc. [deg] |
| pl_orbincllim | VARCHAR2 | Inclination Limit Flag |
| pl_tranmid | NUMBER | Transit Midpoint [days] |
| pl_tranmiderr1 | NUMBER | Transit Midpoint Upper Unc. [days] |
| pl_tranmiderr2 | NUMBER | Transit Midpoint Lower Unc. [days] |
| pl_tranmidlim | VARCHAR2 | Transit Midpoint Limit Flag |
| pl_tsystemref | VARCHAR2 | Time Reference Frame and Standard |
| ttv_flag | VARCHAR2 | Data show Transit Timing Variations |
| pl_imppar | VARCHAR2 | Impact Parameter |
| pl_impparerr1 | VARCHAR2 | Impact Parameter Upper Unc. |
| pl_impparerr2 | VARCHAR2 | Impact Parameter Lower Unc. |
| pl_impparlim | VARCHAR2 | Impact Parameter Limit Flag |
| pl_trandep | NUMBER | Transit Depth [%] |
| pl_trandeperr1 | NUMBER | Transit Depth Upper Unc. [%] |
| pl_trandeperr2 | NUMBER | Transit Depth Lower Unc. [%] |
| pl_trandeplim | VARCHAR2 | Transit Depth Limit Flag |
| pl_trandur | NUMBER | Transit Duration [hours] |
| pl_trandurerr1 | NUMBER | Transit Duration Upper Unc. [hours] |
| pl_trandurerr2 | NUMBER | Transit Duration Lower Unc. [hours] |
| pl_trandurlim | VARCHAR2 | Transit Duration Limit Flag |
| pl_ratdor | NUMBER | Ratio of Semi-Major Axis to Stellar Radius |
| pl_ratdorerr1 | NUMBER | Ratio of Semi-Major Axis to Stellar Radius Upper Unc. |
| pl_ratdorerr2 | NUMBER | Ratio of Semi-Major Axis to Stellar Radius Lower Unc. |
| pl_ratdorlim | VARCHAR2 | Ratio of Semi-Major Axis to Stellar Radius Limit Flag |
| pl_ratror | NUMBER | Ratio of Planet to Stellar Radius |
| pl_ratrorerr1 | NUMBER | Ratio of Planet to Stellar Radius Upper Unc. |
| pl_ratrorerr2 | NUMBER | Ratio of Planet to Stellar Radius Lower Unc. |
| pl_ratrorlim | VARCHAR2 | Ratio of Planet to Stellar Radius Limit Flag |
| pl_occdep | NUMBER | Occultation Depth [%] |
| pl_occdeperr1 | NUMBER | Occultation Depth Upper Unc. [%] |
| pl_occdeperr2 | NUMBER | Occultation Depth Lower Unc. [%] |
| pl_occdeplim | VARCHAR2 | Occultation Depth Limit Flag |
| pl_orbtper | NUMBER | Epoch of Periastron [days] |
| pl_orbtpererr1 | NUMBER | Epoch of Periastron Upper Unc. [days] |
| pl_orbtpererr2 | NUMBER | Epoch of Periastron Lower Unc. [days] |
| pl_orbtperlim | VARCHAR2 | Epoch of Periastron Limit Flag |
| pl_orblper | VARCHAR2 | Argument of Periastron [deg] |
| pl_orblpererr1 | VARCHAR2 | Argument of Periastron Upper Unc. [deg] |
| pl_orblpererr2 | VARCHAR2 | Argument of Periastron Lower Unc. [deg] |
| pl_orblperlim | VARCHAR2 | Argument of Periastron Limit Flag |
| pl_rvamp | NUMBER | Radial Velocity Amplitude [m/s] |
| pl_rvamperr1 | NUMBER | Radial Velocity Amplitude Upper Unc. [m/s] |
| pl_rvamperr2 | NUMBER | Radial Velocity Amplitude Lower Unc. [m/s] |
| pl_rvamplim | VARCHAR2 | Radial Velocity Amplitude Limit Flag |
| pl_projobliq | NUMBER | Projected Obliquity [deg] |
| pl_projobliqerr1 | NUMBER | Projected Obliquity Upper Unc. [deg] |
| pl_projobliqerr2 | NUMBER | Projected Obliquity Lower Unc. [deg] |
| pl_projobliqlim | VARCHAR2 | Projected Obliquity Limit Flag |
| pl_trueobliq | NUMBER | True Obliquity [deg] |
| pl_trueobliqerr1 | NUMBER | True Obliquity Upper Unc. [deg] |
| pl_trueobliqerr2 | NUMBER | True Obliquity Lower Unc. [deg] |
| pl_trueobliqlim | VARCHAR2 | True Obliquity Limit Flag |
